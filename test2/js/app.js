"use strict";

var app = {
    
    init: function(){
        console.log('app | init1');
    },
    
    getResponse: function($a){
        
        var $modal = $($a.data('target'));
        $.ajax({
            url: $a.attr('href'),
            success: function(data){
                $modal.find('.modal-title').text('Response HTML' );
                $modal.find('.modal-body').html(data);
                $modal.modal('show');
            },
            error: function(jqXHR, textStatus, errorThrown){
                $modal.find('.modal-title').text('Error ' + textStatus);
                var alert = $('<div class="alert alert-danger"></div>');
                $modal.find('.modal-body').html(alert.html('Error ' + errorThrown));
                $modal.modal('show');
            },
            dataType: 'html'
        });
    }
    
};

$(document).ready(function() {
    
    app.init();
    
    $(document).on('click','a[data-target="#js-modal"]',function(e){
        e.preventDefault();
        app.getResponse($(this));
    });
    
});




