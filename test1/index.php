<?php
$sms = 'Пароль: 1323
        Спишется 1005,03р.
        Перевод на счет 41001795987001';

function getData(string $message) 
{
    $matches = [];
    $result = [];
    
    preg_match_all('/(?P<password>\d{4}\s)/u', $message, $matches);
    issetIndex($result, $matches, 'password');
    
    preg_match_all('/(?P<amount>\d{4}\,\d{2}р\.)/u', $message, $matches);
    issetIndex($result, $matches, 'amount');
    
    preg_match_all('/(?P<account>\d{14})/u', $message, $matches);
    issetIndex($result, $matches, 'account');
    
    return $result;
}

function issetIndex(&$result, $data, $index) 
{
    if (empty($data[$index][0])) {
        throw new Exception("$index is not defined");
    } else {
        $result[$index] = trim($data[$index][0]);
    }
}

print_r(getData($sms));

echo PHP_EOL;

?>

